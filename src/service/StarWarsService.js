import axios from 'axios';

export class StarWarsService {
    baseUrl = "http://localhost:8080/v1/starWars"
    getAll(){
        return axios.get(this.baseUrl).then(res => res.data);
    }

    save(person){
        return axios.post(this.baseUrl + "/", person).then(res => res.data);
    }
}