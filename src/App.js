import { Component } from 'react';
import './App.css';
import { StarWarsService } from './service/StarWarsService';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Panel} from 'primereact/panel';
import {Menubar} from 'primereact/menubar';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';

import 'primereact/resources/themes/nova/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

export default class App extends Component {

  constructor(){
    super();
    this.state = {
      visible: false,
      person: {
        personid: null,
        name: null,
        height: null,
        mass: null,
        hairColor: null,
        skinColor: null,
        eyeColor: null,
        birthYear: null,
        gender: null
      }
    };

    this.items = [
      {
        label : 'Novo',
        icon : 'pi pi-fw pi-plus',
        command : () => {this.showSaveDialog()}
      },
      {
        label : 'Editar',
        icon : 'pi pi-fw pi-pencil',
        command : () => {alert('Edited!')}
      },
      {
        label : 'Deletar',
        icon : 'pi pi-fw pi-trash',
        command : () => {alert('Deleted!')}
      }
    ];

    this.starWarsService = new StarWarsService();
    this.save = this.save.bind(this);

    this.footer = (
      <div>
        <Button label="Salvar" icon="pi pi-check" onClick={this.save}/>
      </div>
    );
    
  }

  componentDidMount(){
    this.starWarsService.getAll().then(data => this.setState({persons: data}))
  }

  save(){
    this.starWarsService.save(this.state.person).then(data => {
      this.setState({
        visible: false
      });
    })
  }

  render(){
    return (
      <div style={{width:'80%', marginTop: '20px'}}>
        <Menubar model = {this.items}/>
        <Panel header="React Crud APP" >
          <DataTable value={this.state.persons}>
            <Column field = "personid" header="ID"></Column>
            <Column field = "name" header="Nome"></Column>
            <Column field = "height" header="Altura"></Column>
            <Column field = "mass" header="Peso"></Column>
            <Column field = "hairColor" header="Cor do Cabelo"></Column>
            <Column field = "skinColor" header="Cor da Pele"></Column>
            <Column field = "eyeColor" header="Cor dos Olhos"></Column>
            <Column field = "birthYear" header="Birth Year"></Column>
            <Column field = "gender" header="Gênero"></Column>
          </DataTable>
        </Panel>
        <Dialog header="Criar Personagem" visible={this.state.visible} style={{width: '400px'}} footer={this.footer} modal={true}
        onHide={() => this.setState({visible: false})}>
          <form id="person-form">
            <span style={{marginTop:'10px'}} className="p-float-label">
              <InputText value={this.state.person.name} style={{width:'100%'}} id= "name" onChange={(e)=> {
                let val = e.target.value;
                this.setState(prevState => {
                  let person = Object.assign({}, prevState.person)
                  person.name = val

                  return {person};
                })}
              }/>
              <label htmlFor="personid">Nome</label>
            </span>
            
            <span style={{marginTop:'20px'}} className="p-float-label">
              <InputText value={this.state.person.height} style={{width:'100%'}} id= "height" onChange={(e)=> {
                let val = e.target.value;
                this.setState(prevState => {
                  let person = Object.assign({}, prevState.person)
                  person.height = val

                  return {person};
              })}}/>
              <label htmlFor="height">Altura</label>
            </span>

            <span style={{marginTop:'20px'}} className="p-float-label">
              <InputText value={this.state.person.mass} style={{width:'100%'}} id= "mass" onChange={(e)=>{
                let val = e.target.value;
                this.setState(prevState => {
                  let person = Object.assign({}, prevState.person)
                  person.mass = val

                  return {person};
              })}}/>
              <label htmlFor="mass">Peso</label>
            </span>

            <span style={{marginTop:'20px'}} className="p-float-label">
              <InputText value={this.state.person.hairColor} style={{width:'100%'}} id= "hairColor" onChange={(e)=>{
                let val = e.target.value;
                this.setState(prevState => {
                  let person = Object.assign({}, prevState.person)
                  person.hairColor = val

                  return {person};
              })}}/>
              <label htmlFor="hairColor">Cor do Cabelo</label>
            </span>
            
            <span style={{marginTop:'20px'}} className="p-float-label">
              <InputText value={this.state.person.skinColor} style={{width:'100%'}} id= "skinColor" onChange={(e)=>{
                let val = e.target.value;
                this.setState(prevState => {
                  let person = Object.assign({}, prevState.person)
                  person.skinColor = val

                  return {person};
              })}}/>
              <label htmlFor="skinColor">Cor da Pele</label>
            </span>

            <span style={{marginTop:'20px'}} className="p-float-label">
              <InputText value={this.state.person.eyeColor} style={{width:'100%'}} id= "eyeColor" onChange={(e)=>{
                let val = e.target.value;
                this.setState(prevState => {
                  let person = Object.assign({}, prevState.person)
                  person.eyeColor = val

                  return {person};
              })}}/>
              <label htmlFor="eyeColor">Cor do Olho</label>
            </span>

            <span style={{marginTop:'20px'}} className="p-float-label">
              <InputText value={this.state.person.birthYear} style={{width:'100%'}} id= "birthYear" onChange={(e)=>{
                let val = e.target.value;              
                this.setState(prevState => {
                  let person = Object.assign({}, prevState.person)
                  person.birthYear = val

                  return {person};
              })}}/>
              <label htmlFor="birthYear">Birth Year</label>
            </span>

            <span style={{marginTop:'20px'}} className="p-float-label">
              <InputText value={this.state.person.gender} style={{width:'100%'}} id= "gender" onChange={(e)=>{
                let val = e.target.value;
                this.setState(prevState => {
                let person = Object.assign({}, prevState.person)
                person.gender = val

                return {person};
              })}}/>
              <label htmlFor="gender">Gênero</label>
              
            </span>
          </form>
        </Dialog>
      </div>
    )
  }

  showSaveDialog(){
    this.setState({
      visible: true,
      person: {
        personid: null,
        name: null,
        height: null,
        mass: null,
        hairColor: null,
        skinColor: null,
        eyeColor: null,
        birthYear: null,
        gender: null
      }
    });
  }
}